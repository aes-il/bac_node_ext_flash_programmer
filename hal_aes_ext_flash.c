/**
 * Copyright (c) 2016 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @file
 * @defgroup qspi_example_main main.c
 * @{
 * @ingroup qspi_example
 *
 * @brief QSPI Example Application main file.
 *
 * This file contains the source code for a sample application using QSPI.
 */

#include "hal_aes_ext_flash.h"
#include "nrf_drv_qspi.h"
#include "app_error.h"
#include "boards.h"
#include "sdk_config.h"
#include "crc32.h"
#include "nrf_log.h"

#define QSPI_STD_CMD_WRSR   0x01
#define QSPI_STD_CMD_RSTEN  0x66
#define QSPI_STD_CMD_RST    0x99



#define WAIT_FOR_PERIPH() do { \
        while (!m_finished) {} \
        m_finished = false;    \
    } while (0)

static volatile bool m_finished = false;


static void qspi_handler(nrf_drv_qspi_evt_t event, void * p_context)
{
    UNUSED_PARAMETER(event);
    UNUSED_PARAMETER(p_context);
    m_finished = true;
}

static void configure_memory()
{
    uint8_t temporary = 0x40;
    uint32_t err_code;
    nrf_qspi_cinstr_conf_t cinstr_cfg = {
        .opcode    = QSPI_STD_CMD_RSTEN,
        .length    = NRF_QSPI_CINSTR_LEN_1B,
        .io2_level = true,
        .io3_level = true,
        .wipwait   = true,
        .wren      = true
    };

    // Send reset enable
    err_code = nrf_drv_qspi_cinstr_xfer(&cinstr_cfg, NULL, NULL);
    APP_ERROR_CHECK(err_code);

    // Send reset command
    cinstr_cfg.opcode = QSPI_STD_CMD_RST;
    err_code = nrf_drv_qspi_cinstr_xfer(&cinstr_cfg, NULL, NULL);
    APP_ERROR_CHECK(err_code);

    // Switch to qspi mode
    cinstr_cfg.opcode = QSPI_STD_CMD_WRSR;
    cinstr_cfg.length = NRF_QSPI_CINSTR_LEN_2B;
    err_code = nrf_drv_qspi_cinstr_xfer(&cinstr_cfg, &temporary, NULL);
    APP_ERROR_CHECK(err_code);
}





ret_code_t aes_ext_flash_erase(uint32_t start_address,uint32_t length)
{

    //todo - check that address and length are 4kb aligned 
    ret_code_t err_code=NRF_SUCCESS;
    uint32_t erase_len= length;
    uint32_t erase_addr = start_address;
    while (erase_len)
    {
        if ( erase_len>=AES_EF_64KB)
        {
            aes_ext_flash_large_page_erase(erase_addr);
            erase_addr+=AES_EF_64KB;
            erase_len-=AES_EF_64KB;
        }
        else if ( erase_len>=AES_EF_4KB)
        {
            aes_ext_flash_large_page_erase(erase_addr);
            erase_addr+=AES_EF_4KB;
            erase_len-=AES_EF_4KB;
        }
        else
          err_code =NRF_ERROR_INVALID_ADDR;
    }
    return err_code ;
}

ret_code_t aes_ext_flash_large_page_erase(uint32_t start_address)
{
    
    ret_code_t err_code=NRF_SUCCESS;
    m_finished = false;
    err_code = nrf_drv_qspi_erase(NRF_QSPI_ERASE_LEN_64KB, start_address);
    if ( err_code==NRF_SUCCESS)
      WAIT_FOR_PERIPH();
    return err_code;
}


ret_code_t aes_ext_flash_page_erase(uint32_t start_address)
{
    ret_code_t err_code=NRF_SUCCESS;
    m_finished = false;
    err_code = nrf_drv_qspi_erase(NRF_QSPI_ERASE_LEN_4KB, start_address);
    if ( NRF_SUCCESS==err_code)
      WAIT_FOR_PERIPH();
    return err_code;
}

ret_code_t aes_ext_flash_init(nrf_drv_qspi_config_t* config)
{
    ret_code_t err_code;
    m_finished = false;
    nrf_drv_qspi_config_t def_config = NRF_DRV_QSPI_DEFAULT_CONFIG;
    if ( config)
      err_code = nrf_drv_qspi_init(config, qspi_handler, NULL);
    else
      err_code = nrf_drv_qspi_init(&def_config, qspi_handler, NULL);

    if ( NRF_SUCCESS==err_code)
      configure_memory();
    return err_code;
}

ret_code_t aes_ext_flash_write(uint8_t* buffer,uint32_t length,uint32_t start_address)
{
    ret_code_t err_code=NRF_SUCCESS;
    m_finished = false;
    if ( !buffer)
    {
      err_code = NRF_ERROR_INVALID_PARAM;
    }
    else
    {
      err_code = nrf_drv_qspi_write(buffer, length, start_address);
      WAIT_FOR_PERIPH();
    }

    return err_code;
}



ret_code_t aes_ext_flash_read(uint8_t* buffer,uint32_t length,uint32_t start_address)
{
    ret_code_t err_code=NRF_SUCCESS;
    if ( !buffer)
    {
      err_code = NRF_ERROR_INVALID_PARAM;
    }
    else
    {
      err_code = nrf_drv_qspi_read(buffer, length, start_address);
      WAIT_FOR_PERIPH();
    }
    return err_code;
}

void aes_ext_flash_deinit()
{
      nrf_drv_qspi_uninit();

}


uint32_t aes_ext_flash_crc32_compute(uint32_t start_address,uint32_t length)
{
    //assuming we are 256 bytes aligned 
    ret_code_t err_code=NRF_SUCCESS;

    uint32_t accum_crc32=0;
    uint32_t remaining_size = length ;
    uint8_t m_buffer_rx[AES_EF_CRC32_BUFF_SIZE];
    bool first_run=true;

    while (remaining_size>0)
    {
        memset(m_buffer_rx,0,AES_EF_CRC32_BUFF_SIZE);
        int to_copy=AES_EF_CRC32_BUFF_SIZE;
        if ( remaining_size < AES_EF_CRC32_BUFF_SIZE )
          to_copy=remaining_size;
        err_code = aes_ext_flash_read(m_buffer_rx, to_copy, start_address);
        accum_crc32=crc32_compute(m_buffer_rx, to_copy,first_run?NULL:&accum_crc32);
        NRF_LOG_INFO("crc 0x%x ",accum_crc32);
        start_address+=to_copy;
        remaining_size-=to_copy;
        first_run=0;
    }
    return accum_crc32;
}

/** @} */
