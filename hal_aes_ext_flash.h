
#ifndef AES_EXT_FL_H_
#define AES_EXT_FL_H_

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "app_error.h"
#include "nrf_drv_qspi.h"




typedef struct
{
    uint32_t crc32;
    uint32_t image_size;
    uint16_t version;

}node_ext_flash_hdr_t; 


#define AES_EF_64KB (64*1024)
#define AES_EF_4KB (4*1024)
#define AES_EF_256KB (4*AES_EF_64KB)
#define AES_EF_HDR_START_ADDRESS (0)
#define AES_EF_IMAGE_START_ADDRESS (AES_EF_HDR_START_ADDRESS+AES_EF_4KB)
#define AES_EF_CRC32_BUFF_SIZE 256


ret_code_t aes_ext_flash_erase(uint32_t start_address,uint32_t length);
ret_code_t aes_ext_flash_large_page_erase(uint32_t start_address);
ret_code_t aes_ext_flash_page_erase(uint32_t start_address);
ret_code_t aes_ext_flash_init(nrf_drv_qspi_config_t* config);
void aes_ext_flash_deinit();
uint32_t aes_ext_flash_crc32_compute(uint32_t start_address,uint32_t length);

ret_code_t aes_ext_flash_write(uint8_t* buffer,uint32_t length,uint32_t start_address);
ret_code_t aes_ext_flash_read(uint8_t* buffer,uint32_t length,uint32_t start_address);
#endif  //AES_EXT_FL_H_
