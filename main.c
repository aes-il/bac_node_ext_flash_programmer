/**
 * Copyright (c) 2016 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @file
 * @defgroup qspi_example_main main.c
 * @{
 * @ingroup qspi_example
 *
 * @brief QSPI Example Application main file.
 *
 * This file contains the source code for a sample application using QSPI.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "app_error.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "crc32.h"
#include "sw_version.h"
#include "hal_aes_ext_flash.h"



extern unsigned int image_bin_len;
extern const unsigned char image_bin[];
static uint8_t m_buffer_tx[AES_EF_CRC32_BUFF_SIZE];


int main(void)
{
    uint32_t i;
    uint32_t err_code;
    node_ext_flash_hdr_t hdr={0};
    err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);
    NRF_LOG_DEFAULT_BACKENDS_INIT();
    NRF_LOG_INFO("Spider Node version %d ext Programmer",NODE_VERSION);


    //init extrnal flash using HAL
    if ( aes_ext_flash_init(NULL)==NRF_SUCCESS)
    {
        NRF_LOG_INFO("Process of erasing start");
        aes_ext_flash_erase(0,AES_EF_256KB);
    }
    else  
      return 0;

    //to simulate an image, we have a node image compiled into a byte array, image.c
    //we write this image to flash
    //image_bin_len is the image length - defined in image.c
    
    uint8_t const * ptr=&image_bin[0]; //point to the byte array 
    // first page 4KB contains header for the image stored in flash 
    // the header contain crc anc file size
    hdr.crc32= crc32_compute(ptr,image_bin_len, NULL); // calculate crc 
    hdr.image_size=image_bin_len;
    hdr.version=NODE_VERSION;

    
    //go over the byte array and write to flash 
    NRF_LOG_INFO("Process of writing data start");
    int start_address = AES_EF_IMAGE_START_ADDRESS;
    int remaining_size=image_bin_len;
    while (remaining_size>0)
    {
        int to_copy=AES_EF_CRC32_BUFF_SIZE;
        if ( remaining_size < AES_EF_CRC32_BUFF_SIZE )
          to_copy=remaining_size;

        memset(m_buffer_tx,0,AES_EF_CRC32_BUFF_SIZE); //zero the buffer
        memcpy(m_buffer_tx,ptr,to_copy);
        err_code = aes_ext_flash_write(m_buffer_tx, to_copy, start_address);
        APP_ERROR_CHECK(err_code);
        remaining_size-=to_copy;
        start_address+=to_copy;
        ptr+=to_copy; //advance the image in byte array 
    }

    //now - lets verify image was written succesfully by calculating the image crc  
    // and comparing it to the one we've calculated in the beginning 
    start_address=AES_EF_IMAGE_START_ADDRESS;
    uint32_t accum_crc32 =aes_ext_flash_crc32_compute(start_address,image_bin_len);
    NRF_LOG_INFO("crc 0x%x , calculated crc 0x%x",hdr.crc32,accum_crc32);
    if ( hdr.crc32==accum_crc32)
      NRF_LOG_INFO("SUCCESS");


    //update the heaser with the crc
    err_code = aes_ext_flash_write((uint8_t*)&hdr, sizeof(hdr),AES_EF_HDR_START_ADDRESS);
    APP_ERROR_CHECK(err_code);
    aes_ext_flash_deinit();

    for (;;)
    {
    }
}
